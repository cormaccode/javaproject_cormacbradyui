import React from 'react';
import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";
import {Switch, Route, Link} from "react-router-dom";
import ListPaymentRedux from "./components/ListPaymentRedux"
import AddPaymentRedux from "./components/AddPaymentRedux"
import About from "./components/About"
import Welcome from "./components/Welcome"

function App() {
  return (

    <div id="container">
      <nav className="navbar navbar-expand navbar-dark bg-dark">
        <a href="/home" className="navbar-brand">
          Home
        </a>
        <div className="navbar-nav mr-auto">
          <li className="nav-item">
            <Link to={"/addpaymentredux"} className="nav-link">
              Add Payment Redux
            </Link>
          </li>
          <li className="nav-item">
            <Link to={"/viewallpaymentsredux"} className="nav-link">
              View All Payments Redux
            </Link>
          </li>
          <li className="nav-item">
            <Link to={"/about"} className="nav-link">
              About
            </Link>
          </li>
        </div>
      </nav>
      <div className="container mt-3">
        <Switch>
          <Route exact path={["/", "/home"]} component={Welcome} />
          <Route exact path="/addpaymentredux" component={AddPaymentRedux} />
          <Route exact path="/viewallpaymentsredux" component={ListPaymentRedux} />
          <Route exact path="/about" component={About} />
        </Switch>
      </div>

    </div>

  );
}

export default App;
