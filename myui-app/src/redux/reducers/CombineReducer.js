import { combineReducers } from "redux";

import PaymentAddReducer from "./PaymentAddReducer";

import PaymentListReducer from "./PaymentListReducer";

export default combineReducers({
    PaymentAddReducer: PaymentAddReducer,
    PaymentListReducer: PaymentListReducer
})
