
import {PAYMENT_ADD_REQUEST , PAYMENT_ADD_SUCCESS , PAYMENT_ADD_FAIL}
    from "../constants/PaymentConstants"

import PaymentService from "../../services/PaymentRestService";

const paymentAdd = (payment) => async (dispatch) => {
    try {
        dispatch({ type: PAYMENT_ADD_REQUEST, payload: payment });

        const newPayment = await PaymentService.create(payment);

        dispatch({ type: PAYMENT_ADD_SUCCESS, payload: newPayment });
    } catch (error) {
        dispatch({ type: PAYMENT_ADD_FAIL, payload: error });
    }
};

export default paymentAdd;