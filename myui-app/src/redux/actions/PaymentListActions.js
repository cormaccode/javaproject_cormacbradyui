import {PAYMENT_LIST_REQUEST , PAYMENT_LIST_SUCCESS , PAYMENT_LIST_FAIL}
    from "../constants/PaymentConstants"

import PaymentService from "../../services/PaymentRestService";

const paymentList = () =>  (dispatch) => {
    try {
        dispatch({ type: PAYMENT_LIST_REQUEST });
        PaymentService.getAll().then(response => {
            dispatch({ type: PAYMENT_LIST_SUCCESS, payload: response.data });
        });

    } catch (error) {
        dispatch({ type: PAYMENT_LIST_FAIL, payload: error });
    }
};
export default paymentList;
