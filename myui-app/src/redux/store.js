import reducer from "./reducers/CombineReducer";
import PaymentAddReducer from "./reducers/PaymentAddReducer";
import PaymentListReducer from "./reducers/PaymentListReducer";

import {applyMiddleware, createStore} from "redux";
import thunk from "redux-thunk";
const initialState = {};
export const middlewares = [thunk];
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ //|| compose;

 const store = createStore(
     PaymentListReducer,//typically combine all reducers
     initialState,
     composeEnhancer(applyMiddleware(...middlewares))
);

export default store;