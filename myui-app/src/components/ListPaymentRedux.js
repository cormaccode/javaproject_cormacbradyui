import React, {Component} from 'react';
import paymentList from "../redux/actions/PaymentListActions";
import { connect } from 'react-redux';

class ListPaymentRedux extends  Component {
    constructor(props) {
        super(props)
        this.props.dispatch(paymentList())
    }

    render() {
        const {payments} = this.props;
        return (
            <div className="col-md-6">
                <h4>Payment List Redux</h4>
              <b><em> (Click on payment to view additional info about that payment)</em></b>
<br /><br />
                <ul className="list-group">
                    {payments &&
                    payments.map((payment, index) => (
                        <li
                            key={index}
                            onClick={()=>{
                                alert('Customer ID : ' +  payment.custid +
                                ' Payment Date : ' + payment.paymentdate )}}
                        >
                           <b>ID:</b> {payment.id }  <b>Payment Type:</b> {payment.type} <b>Amount:</b> {payment.amount}
                        </li>
                    ))}
                </ul>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch: () => dispatch(paymentList())
    };
};

const mapStateToProps = state => ({
    error:state.error,
    payments: state.payments,
    pending: state.pending
})

export default connect(mapStateToProps,mapDispatchToProps)(ListPaymentRedux);
