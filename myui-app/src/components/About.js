import React from 'react';
import PaymentService from "../services/PaymentRestService";

const About = () => {
    const [status, setStatus] = React.useState([]);

    React.useEffect(() => {
        const fetchData = async () => {
            setStatus("Long running process in progress")
            const response = await PaymentService.status();
            setStatus(response.data);
        }
        fetchData();
    }, []);

    return (

        <div className="container">

            <span className="child blinking">{status}</span>
        </div>



    );
}

export default About;