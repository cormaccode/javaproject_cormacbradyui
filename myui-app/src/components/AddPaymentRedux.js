import React, { useState } from "react";
import paymentAdd from "../redux/actions/PaymentAddActions";
import {useDispatch} from "react-redux";

function AddPaymentRedux() {
    const dispatch = useDispatch();
    const initialFormState = {
        id: 0,
        paymentdate: "",
        type: "",
        amount: "",
        custid:0,
        submitted: false };

    //use State takes the inital state and a function to update it
    const [payment, setPayment] = useState(initialFormState);

    const handleInputChange = (event) => {
        const { name, value } = event.target;
        setPayment({ ...payment, [name]: value });
    };

    return (<form
            onSubmit={(event) => {
                //prevent submitting which is default behaviour
                event.preventDefault();
                //send the payload which is state names payment
                dispatch(paymentAdd(payment));
            }}>
            <h1>Add Payment</h1>
            <div className="form-group">
                <label htmlFor="id">ID</label>
                <input type="text" className="form-control" id="id"  name="id" aria-describedby="idHelp"
                       placeholder="Enter ID" value={payment.id} required onChange={handleInputChange}/>

            </div>
            <div className="form-group">
                <label htmlFor="paymentdate">Payment Date</label>
                <input type="text" className="form-control" id="paymentdate" name="paymentdate" aria-describedby="paymentdateHelp"
                       placeholder="Enter Date eg (1970-01-01)" value={payment.paymentdate} required onChange={handleInputChange}/>

            </div>
            <div className="form-group">
                <label htmlFor="type">Type of Payment</label><br />
                <select id="type" required onChange={handleInputChange}>
                    <option value="Full Payment">Full Payment</option>
                    <option value="Half Payment">Half Payment</option>
                    <option value="Flexible Payment">Flexible Payment</option>

                </select>


            </div>
            <br /><br />
            <div className="form-group">
                <label htmlFor="amount">Amount</label>
                <input type="number" className="form-control" id="amount"  name="amount" aria-describedby="amountHelp"
                       placeholder="Enter amount" value={payment.amount} required onChange={handleInputChange}/>

            </div>
            <div className="form-group">
                <label htmlFor="custid">Customer ID</label>
                <input type="number" className="form-control" id="custid" name="custid" aria-describedby="custidHelp"
                       placeholder="Enter custid" value={payment.custid} onChange={handleInputChange}/>

            </div>
            <div className="form-group">

                <input type="submit" className="mybtn" value="Save" ></input>
            </div>

        </form>
    );
}

export default AddPaymentRedux;
