import http from "../http-common";
export class PaymentService {
    getAll() {
        return http.get("/all");
    }

    get(id) {
        return http.get(`/find/${id}`);
    }

    create(data) {
        return http.post("/addpayment", data);
    }

    status() {
        return http.get("/status");
    }
}

export default new PaymentService();