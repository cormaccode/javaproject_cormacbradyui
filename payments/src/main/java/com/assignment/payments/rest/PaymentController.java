package com.assignment.payments.rest;

import com.assignment.payments.entities.Payment;
import com.assignment.payments.services.IPaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api")
public class PaymentController implements IPaymentController {
    private IPaymentService service;
    private static final Logger logger = LoggerFactory.getLogger(PaymentController.class);

    public PaymentController(IPaymentService service){
        this.service = service;
    }

    @Override
    @GetMapping("/count")
    public ResponseEntity<Integer> getRowCount()
    {
        logger.info("PaymentController - Inside the 'getRowCount'' method");
        return new ResponseEntity<>(service.rowcount(), HttpStatus.OK);
    }

    @Override
    @GetMapping("/id")
    public ResponseEntity<Payment> getPaymentById(@RequestParam int id)

    {
        logger.info("PaymentController - Inside the 'getPaymentById'' method - " + id);
        return new ResponseEntity<>(service.findById(id), HttpStatus.OK);
    }

    @Override
    @GetMapping("/type")
    public ResponseEntity<List<Payment>> getPaymentByType(@RequestParam String type)

    {
        logger.info("PaymentController - Inside the 'getPaymentByType'' method - " + type);
        return new ResponseEntity<>(service.findByType(type), HttpStatus.OK);
    }

    @Override
    @RequestMapping(value = "/addpayment", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity addPayment(@RequestBody Payment payment) {
        logger.info("PaymentController - Inside the 'addPayment' method - " + payment.toString());
        service.save(payment);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @Override
    @GetMapping("/status")
    public ResponseEntity getStatus()

    {
        logger.info("PaymentController - Inside the 'getStatus'' method");
        return new ResponseEntity<>(HttpStatus.OK);
    }
}