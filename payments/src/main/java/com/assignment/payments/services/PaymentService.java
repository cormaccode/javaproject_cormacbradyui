package com.assignment.payments.services;

import com.assignment.payments.dao.IPaymentDAO;
import com.assignment.payments.entities.Payment;
import com.assignment.payments.exceptions.CustomExceptions;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class PaymentService implements IPaymentService{

    private IPaymentDAO iPaymentDAO;

    public PaymentService(IPaymentDAO iPaymentDAO){
        this.iPaymentDAO =  iPaymentDAO;
    }

    @Override
    public int rowcount() {
        return iPaymentDAO.rowcount();
    }

    @Override
    public Payment findById(int id) {
        if(id<1) throw new CustomExceptions("Id must be above zero");
        return iPaymentDAO.findById(id);
    }

    @Override
    public List<Payment> findByType(String type) {
        if(type == null || type.isEmpty() ) throw new CustomExceptions("type cannot be null or empty");
        return iPaymentDAO.findByType(type);
    }

    @Override
    public int save(Payment payment) {
        return iPaymentDAO.save(payment);
    }
}
