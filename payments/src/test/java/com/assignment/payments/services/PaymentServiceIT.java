package com.assignment.payments.services;

import com.assignment.payments.entities.Payment;
import com.assignment.payments.exceptions.CustomExceptions;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
@SpringBootTest
public class PaymentServiceIT {

    //Unable to constructor autowiring here need to check why
    @Autowired
    IPaymentService iPaymentService;
    @Autowired
    MongoTemplate mongoTemplate;

    //just put this into one test as the others can get date passed in
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    Date now = new Date();
    private String date;

    @BeforeEach
    public void setup() {
        date = simpleDateFormat.format(now);
        mongoTemplate.dropCollection("payment");
    }

    @Test
    public void savePaymentToDB() throws ParseException {
        Payment actual = new Payment(1, simpleDateFormat.parse(date), "Full Payment", 10.0, 1);
        iPaymentService.save(actual);

        Payment expected = iPaymentService.findById(1);
        assertNotNull(expected);
    }

    @Test
    public void getRowCountFromDB() {
        Payment actual = new Payment(1, new Date(), "Full Payment", 10.0, 1);
        iPaymentService.save(actual);
        assertEquals(1, iPaymentService.rowcount());
    }

    @Test
    public void retrievePaymentById() {
        Payment actual = new Payment(1, new Date(), "Full Payment", 10.0, 1);
        iPaymentService.save(actual);

        Payment expected = iPaymentService.findById(1);
        assertEquals(expected.toString(), actual.toString());
    }

    @Test
    public void retrieveListOfPaymentsByType() {
        Payment paymentOne = new Payment(1, new Date(), "Full Payment", 10.0, 1);
        Payment paymentTwo = new Payment(2, new Date(), "Full Payment", 20.0, 2);

        iPaymentService.save(paymentOne);
        iPaymentService.save(paymentTwo);

        List<Payment> expected = new ArrayList<>();
        expected.add(paymentOne);
        expected.add(paymentTwo);

        List<Payment> actual = iPaymentService.findByType("Full Payment");
        assertEquals(expected.size(), actual.size());
    }

    @Test
    public void retrievePaymentByIdThrowsExceptionWhenLessThanZero() {
        Payment actual = new Payment(0, new Date(), "Full Payment", 10.0, 1);
        iPaymentService.save(actual);

        Assertions.assertThrows(CustomExceptions.class, () -> {
            iPaymentService.findById(0);
        });
    }

    @Test
    public void retrieveListOfPaymentsByTypeWhenTypeIsNull() {
        Payment paymentOne = new Payment(1, new Date(), "Full Payment", 10.0, 1);
        Payment paymentTwo = new Payment(2, new Date(), "Full Payment", 20.0, 2);

        iPaymentService.save(paymentOne);
        iPaymentService.save(paymentTwo);

        List<Payment> expected = new ArrayList<>();
        expected.add(paymentOne);
        expected.add(paymentTwo);

        Assertions.assertThrows(CustomExceptions.class, () -> {
        iPaymentService.findByType(null); });
    }
}
