package com.assignment.payments.dao;

import com.assignment.payments.entities.Payment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
public class PaymentDaoIT {

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    IPaymentDAO iPaymentDAO;

    @BeforeEach
    public void setup(){
        mongoTemplate.dropCollection("payment");
    }

    @Test
    public void savePaymentToDB(){
        Payment actual = new Payment(1,new Date(),"Full Payment", 10.0, 1 );
        iPaymentDAO.save(actual);

        Payment expected = iPaymentDAO.findById(1);
        assertNotNull(expected);
    }

    @Test
    public void getRowCountFromDB(){
        Payment actual = new Payment(1,new Date(),"Full Payment", 10.0, 1 );
        iPaymentDAO.save(actual);
        assertEquals(1, iPaymentDAO.rowcount());
    }

    @Test
    public void retrievePaymentById(){
        Payment actual = new Payment(1,new Date(),"Full Payment", 10.0, 1 );
        iPaymentDAO.save(actual);

        Payment expected = iPaymentDAO.findById(1);
        assertEquals(expected.toString(), actual.toString());
    }

    @Test
    public void retrieveListOfPaymentsByType(){
        Payment paymentOne = new Payment(1,new Date(),"Full Payment", 10.0, 1 );
        Payment paymentTwo = new Payment(2,new Date(),"Full Payment", 20.0, 2 );

        iPaymentDAO.save(paymentOne);
        iPaymentDAO.save(paymentTwo);

        List<Payment> expected = new ArrayList<>();
        expected.add(paymentOne);
        expected.add(paymentTwo);

        List<Payment> actual = iPaymentDAO.findByType("Full Payment");
        assertEquals(expected.size(),actual.size());
    }
}
